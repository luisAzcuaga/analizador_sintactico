package analizador;

public class Backend {

	String[] result;
	public String analisis(String data) {
		//String is received and transformed to upperCase
		String preProceso = data.toUpperCase(); //Original string
		//Process begins with the splitting of the original string into separate strings
		String[] lines = preProceso.split("\r\n|\r|\n");
		for (int q=0; q<lines.length; q++) {
			lines[q] = lines[q].trim();
		}
		String type[] = new String[0];
		String variables[] = new String [0];
		String value[] = new String[0];
		boolean thereAreNotDeclarations = true;
		int position1=0;
		int position2 =0;
		boolean allGood = true;
		String Result ="";


		for (int x = 0 ; x < lines.length ; x++) {
			int cont = 0;
			int begin = 0;

			if (lines[x].endsWith(";")){
				for (int y = 0 ; y < lines[x].length() ; y++) {
					if (lines[x].length()>5 && lines[x].substring(0, 3).equals("INT") || lines[x].length()>5 && lines[x].substring(0, 5).equals("FLOAT") || lines[x].length()>6 && lines[x].substring(0, 6).equals("STRING")) { //If this IS a declaration line, read it.

						value = this.arrayStrPush("null", value); //Start filling variable value as null, might be changed later

						if (Character.isDigit(lines[x].charAt(y))) {
							String thisSubString = (lines[x].substring(y, lines[x].length()-1));
							value[variables.length-1] = thisSubString;
							break;
						} else if (!Character.isLetter(lines[x].charAt(y))) { //Is it no long a letter, lets check now what is it
							String thisSubString =lines[x].substring(begin, y); //Get whatever you read since the last 'no-letter' found
							if (cont==0) {
								type = this.arrayStrPush(thisSubString, type);
							}
							if (cont==1) {
								variables = this.arrayStrPush(thisSubString, variables);
							}
							cont++;
							begin = y+1; //Update the begin value for the next reading
						}//End else-if
					} //End If detect variable type
				} //End For Y
			} else if (!lines[x].contains("FOR") ) {
				Result +="Falto un punto y coma (;) en la linea: "+ (x+1) + "\n";
				allGood = false;
			}
		} //End For X

		if (allGood) {
			String[] terms;
			//FOR section
			int brackets = 0;
			for (int n = 0 ; n < lines.length ; n++) { // Y axis
				if ( lines[n].length() > 3 && lines[n].substring(0, 3).equals("FOR") ){ //Line begins with for
					if (lines[n].endsWith("{")){ //Last character from line is a left bracket thus is opening a for
						brackets++;
						if (lines[n].substring(4,5).equals("(") ){ //Character from line is a opening parenthesis thus is opening for conditions
							if (lines[n].substring(lines[n].length()-2,lines[n].length()-1).equals(")") ){ //Character from line is a closing parenthesis thus is closing for conditions
								terms = (lines[n].substring(5, lines[n].length()-2).split(";\\s+|;")); //Splits the content of the for, for example:  for ( ->>> int i = 0 ; i <10; i++  <<<- )
								//[0]-> int i = 0
								//[1]-> i<10
								//[2]-> i++
								if (terms.length == 3) {
									//Splitting first parameter
									boolean found = false;
									String temp[] = terms[0].split("[=\\s]"); // int i = 0 ->  [0]-> INT  [1]-> I   [2]-> 0
									for (int q=0; q<terms.length; q++) {
										terms[q] = terms[q].trim();
									}
									if (temp.length == 3) {
										if (temp[0].equals("")) {
											for (int i = 0 ; i < variables.length ; i++) {
												if(variables[i].equals(temp[1])) {
													found = true;
													value[i] = temp[2];
												}
											}
											if (!found) {
												Result+=temp[1] + ": No esta declarada! Linea: "+ (n+1) +"\n";
												allGood = false;
												break;
											}
										} else {										
											type = this.arrayStrPush(temp[0], type);
											variables = this.arrayStrPush(temp[1], variables);
											if (value.length == 0) {
												value = this.arrayStrPush(temp[2], value);
											} else {
												if (temp.length >= 3) {											
													value[variables.length-1] = temp[2];
												} else {
													allGood = false;
													Result += "Error en declaraciones del for.\n";
												}
											}
										}
									} else {
										for (int i = 0 ; i < variables.length ; i++) {
											if(variables[i].equals(temp[0])) {
												found = true;
												value[i] = temp[1];
											}
										}
										if (!found) {
											Result+=temp[0] + ": No esta declarada! Linea: "+ (n+1) +"\n";
											allGood = false;
											break;
										}
									}
									//Splitting second parameter
									temp = terms[1].split("\\s?(?:<=?|>=?|==|!=)\\s?");
									found = false;
									for (int b=0;b<temp.length;b++) {
										for (int i = 0 ; i < variables.length ; i++) {
											if(variables[i].equals(temp[0])) {
												found = true;
											}
										}
										if (!found) {
											Result+=temp[0] + ": 2.1 param. No esta declarada! Linea: "+ (n+1) +"\n";
											allGood = false;
											break;
										}
									}
									if (!temp[1].matches("-?\\d+(\\.\\d+)?")) {
										found = false;
										for (int b=0;b<temp.length;b++) {
											for (int i = 0 ; i < variables.length ; i++) {
												if(variables[i].equals(temp[1])) {
													found = true;
												}
											}
											if (!found) {
												Result+=temp[1] + ": 2.2 param. No esta declarada! Linea: "+ (n+1) +"\n";
												allGood = false;
												break;
											}
										}
									}
									//Splitting third parameter
									temp = terms[2].split("[++]");
									found = false;
									for (int i = 0 ; i < variables.length ; i++) {
										if(variables[i].equals(temp[0])) {
											found = true;
										}
									}
									if (!found) {
										Result+=temp[0] + ": 3 param. No esta declarada! Linea: "+ (n+1) +"\n";
										allGood = false;
										break;
									}
								} else {
									Result += "Hubo un problema con el for en la linea: " + (n+1) + "\n";
									allGood = false;
									break;
								}
							} else {
								Result+="Se esperaba un ')' en la linea: " + (n+1) + "\n";
								allGood = false;
								break;
							}
						} else {
							Result+="Se esperaba una parentesis ( en la linea: " + (n+1) + "\n";
							allGood = false;
							break;
						}
					} else {
						Result+="Se esperaba una llave \"{\" en la linea: " + (n+1) + "\n";
						allGood = false;
						break;
					}
				} else {
					//System.out.println(":)");
				}
				if (lines[n].endsWith("};")){ //Last character from line is a right bracket thus is closing a for
					brackets--;
				}
			}
			if (brackets != 0 ) {
				Result+="No todas las llaves fueron cerradas!";
				allGood = false;
			}
		}

		if (allGood) {
			// Variable reader
			if (type.length != 0 && variables.length != 0) {
				thereAreNotDeclarations=false;
				for (int aux = 0; aux < type.length ; aux++) {
					Result += "Tipo: " + type[aux] + "\tLexema: " + variables[aux] + "\tValor: " + value[aux] + "\n";
				}
			} else {
				Result +="No hubieron declaraciones! :(\n";
			}
			Result +="----------\n";
		}

		if (allGood) {
			//Restarting process for arithmetic operations
			String operando_1[] = new String[0];
			String operando_2[] = new String [0];
			if (!thereAreNotDeclarations) { //IF OPERANDS ARE ONLY NUMBERS THIS VARIABLE COULD BE TRUE
				for (int z = 0 ; z < lines.length ; z++) {
					int begin = 0;
					int cont = 0;
					for (int y = 1 ; y < lines[z].length() ; y++) { //Starts from 1 otherwise there wouldn't be anything to get on substring 0,0
					String thisSubString = lines[z].substring(begin, y); //Get that possible operand
					if (!lines[z].contains("INT") && !lines[z].contains("FLOAT") && !lines[z].contains("STRING") && !lines[z].contains("FOR") && !lines[z].contains("}"))  { //If this is NOT a var declaration line, read it.
						if (!thisSubString.contains("+") && !thisSubString.contains("-") && !thisSubString.contains("*") && !thisSubString.contains("/") && !thisSubString.contains("%") ) {
							if (cont==0) {
								boolean found = false;
								if (!thisSubString.matches("-?\\d+(\\.\\d+)?")) {
								for (int i = 0 ; i < variables.length ; i++) {
									if(variables[i].equals(thisSubString)) {
										operando_1 = this.arrayStrPush(thisSubString, operando_1);
										position1 = i;
										found = true;
									}
								}
								if (!found) {
									Result+=thisSubString + ": No esta declarada!(Operando 1)\n";
									break;
								}
								} else {
									//operando_1 = this.arrayStrPush(lines[z].substring(begin, thisSubString.length()), operando_1);
									operando_1 = this.arrayStrPush(thisSubString, operando_1);
									y+=thisSubString.length();
								}
								
							}
							if (cont==1) {
								boolean found = false;
								if (!thisSubString.matches("-?\\d+(\\.\\d+)?")) {
								for (int n = 0 ; n < variables.length ; n++) {
									if(variables[n].equals(thisSubString)) {
										operando_2 = this.arrayStrPush(thisSubString, operando_2);
										position2 = n;
										found = true;
									}
								}
								if (!found) {
									Result+=thisSubString + ": No esta declarada!(Operando 2)\n";
									break;
								}
							} else {
								operando_2 = this.arrayStrPush(thisSubString, operando_2);
								y+=thisSubString.length();
							}
								
							}
							cont++;
						}
					}
					begin = y; //Update the begin value for the next reading
				}
			} //End For Y
		} //End For Z
		if (type.length != 0 && operando_1.length > 0 && operando_2.length > 0) { //There are declarations
			if (operando_1.length > 0 && operando_2.length > 0) { //There ARE operations
				for (int aux = 0; aux < operando_1.length ; aux++) {
					Result+="Operando 1: " + operando_1[aux] + "\t\tOperando 2: " + operando_2[aux] + "\n";
				}
				if (type.length > 1 && !type[position1].equals(type[position2])) { //Same type?
					//Result+="\nTipo 1: " + type[position1] + " Tipo 2: " + type[position2] + "\n";
					Result+="Incompatibilidad de tipo!\n";
				}
			} else {
				Result+="No se hicieron operaciones!\n";
			}
		}
	}
	Result+="\n";

	if (!allGood) {
		Result+= "ERROR.";
	}
	return Result;
	}

	public String[] arrayStrPush(String item, String[] oldArray) {
		int len = oldArray.length;
		String[] newArray = new String[len+1];
		System.arraycopy(oldArray, 0, newArray, 0, len);
		newArray[len] = item;

		return newArray;
	}
	public int[] arrayIntPush(int item, int[] oldArray) {
		int len = oldArray.length;
		int[] newArray = new int[len+1];
		System.arraycopy(oldArray, 0, newArray, 0, len);
		newArray[len] = item;

		return newArray;
	}
}
