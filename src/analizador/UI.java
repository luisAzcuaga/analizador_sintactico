package analizador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.Font;

public class UI extends JFrame {

	private JPanel contentPane;
	Backend backend = new Backend();
	
	String extract = "";
	String result = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = new UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UI() {
		setResizable(false);
		setTitle("Proyecto Lenguajes y Automatas II [Por Luis Azcuaga y Ricardo Meza]");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JTextArea input = new JTextArea();
		input.setBounds(10, 10, 183, 296);
		panel.add(input);
		
		JTextArea resultArea = new JTextArea();
		resultArea.setFont(new Font("Monospaced", Font.PLAIN, 11));
		resultArea.setEditable(false);
		resultArea.setBounds(203, 15, 321, 325);
		panel.add(resultArea);
		
		JButton btnAnalizar = new JButton("\u00A1Analizar!");
		btnAnalizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				UI.this.extract = input.getText();
				result = UI.this.backend.analisis(UI.this.extract);
				resultArea.setText(result);
			}
		});
		btnAnalizar.setBounds(52, 317, 89, 23);
		panel.add(btnAnalizar);
		
		
	}
}
